# Linear Regression from Scratch

Linear Regression from Scratch in Python



#### The simplest form of the regression equation with one dependent and one independent variable.

# y = m * x + b

where

1. _y = estimated dependent value._ 
2. _b = constant or bias._ 
3. _m = regression coefficient or slope._ 
4. _x = value of the independent variable._ 

### Linear Regression from Scratch

 Linear Regression from scratch using only Math 

 ![alt text](https://www.askpython.com/wp-content/uploads/2020/12/MSE-300x83.gif.webp)

 A mean squared error function as the name suggests is the mean of squared sum of difference between true and predicted value.

> As the predicted value of y depends on the slope and constant, hence our goal is to find the values for slope and constant that minimize the loss function or in other words, minimize the difference between y predicted and true values.

1. **Optimization Algorithm**
Optimization algorithms are used to find the optimal set of parameters given a training dataset that minimizes the loss function, in our case we need to find the optimal value of slope (m) and constant (b).


### One such Algorithm is Gradient Descent.
Gradient descent is by far the most popular optimization algorithm used in machine learning.

Using gradient descent we iteratively calculate the gradients of the loss function with respect to the parameters and keep on updating the parameters till we reach the local minima.



## OutPut:
![alt text](https://www.askpython.com/wp-content/uploads/2020/12/Prediction-Linear-Regression-1536x922.jpeg.webp)

