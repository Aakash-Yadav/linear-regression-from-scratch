from math import pow ,sqrt

class simple_linear_regression(object):    
    def __init__(self):
        self.x = [i for i in range(1,6)]
        self.y = [pow(x,2) for x in range(1,6)] 
        self.x_bar = sum(self.x)/len(self.x)
        self.y_bar = sum(self.y)/len(self.y)
    
    def x_sub_x_bar(self,num):
        self.num = num 
        return self.num - self.x_bar
    
    def y_sub_y_bar(self,num):
        self.num = num 
        return self.num - self.y_bar
    
    def list_x_y_bar(self):
        x_data = list(map(self.x_sub_x_bar,self.x))
        y_data =list(map(self.y_sub_y_bar,self.y))
        return x_data , y_data
    
    def product(self):
        out_list_of_sum = []
        for i , j in zip(self.list_x_y_bar()[0],self.list_x_y_bar()[-1]):
            out_list_of_sum.append(i*j)
        x_bar_pow = [pow(i,2) for i in self.list_x_y_bar()[0]]
        return sum(out_list_of_sum)/sum(x_bar_pow)


class main_solving(simple_linear_regression):
    'formula  y = b + b1 x'

    def solving(self):
        self.B = self.product()*self.x_bar
        return (self.y_bar - self.B )
    def fit(self,num):
        return self.solving() +self.product() *num
    
    def Y_astimet(self):
        return list(map(self.fit,self.x))
    
    def distance(self):
        y_y_bar = []
        for i , j in zip(self.y , self.Y_astimet()):
            y_y_bar.append(pow(j-i,2))
        a = len(self.x)-2
        return (sqrt(sum(y_y_bar)/a))

if __name__ == '__main__':
    print(main_solving().fit(10))

